### Aladdin CMS  
[![license](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat)](https://gitlab.com/huaiping/aladdin/blob/master/LICENSE) [![Build Status](https://travis-ci.com/huaiping/aladdin.svg?branch=master)](https://travis-ci.com/huaiping/aladdin) [![Coverage Status](https://coveralls.io/repos/github/huaiping/aladdin/badge.svg?branch=master)](https://coveralls.io/github/huaiping/aladdin?branch=master)  
Open Source CMS. Just for Practice.

### Requirements
PHP 5.6+ [https://php.net](https://php.net)  
MySQL 5.5+ [https://www.mysql.com](https://www.mysql.com)

### Features
HTML 5 + CodeIgniter 3.1.10 + Bootstrap 3.4.0

### Demo
[https://www.huaiping.net](https://huaiping.net/v2/)

### License
Licensed under the [MIT License](https://gitlab.com/huaiping/aladdin/blob/master/LICENSE).
